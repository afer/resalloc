using DifferentialEquations, DataFrames, Plots, DiffEqCallbacks, Random, BlackBoxOptim, Sundials, Dates, GLM

include("model.jl")
include("parameters.jl")
include("functions.jl")

species = [:a, :em, :et, :mm, :mq, :mr, :mt, :eq, :er, :rm1m, :rm1q, :rm1r, :rm1t, :rm2m, :rm2q, :rm2r, :rm2t, :si, :zmm, :zmq, :zmr, :zmt]
tspan = (0.,1e5)

cb = TerminateSteadyState(1e-6, 1e-4)
prob = ODEProblem(ribofinal!,init,tspan, (rates,parameters))
using BenchmarkTools
# Seems to be considerably slower (2x), so I'll manually solve using Rosenbrock
#@btime sol = solve(prob, alg=DynamicSS(Rosenbrock23(), abstol=1e-6, reltol=1e-4, tspan=Inf), save_start=false, save_everystep=false)
@btime sol = solve(prob, alg=Rosenbrock23(), abstol=1e-4, save_start=false, save_everystep=false; callback=cb)
# solDF = DataFrame([[j[i] for j in sol.u] for i=1:length(sol.u[1])], species)
# plot(sol.t,[i[10] for i in sol.u])

# obtaindata(solDF[end,:], parameters)

# divRates = []
# ribofractions = []
# for t=1:length(sol.t)
#     push!(divRates, calculateDivRate(solDF[t,:]))
#     push!(ribofractions, calculateMFAllocation(solDF[t, :])["Ribosomal"])
# end

 nsRange = 0.15:0.1:0.9
# rhoRange = 0.01:0.1:1
# n = length(rhoRange)
# lamValues =zeros(length(nsRange),length(rhoRange))
# mfa = zeros(length(nsRange), length(rhoRange))
# for (i,rhoVal) in enumerate(rhoRange)
#     rates[4] = rhoVal
#     println(rhoVal)
#   for (j, nsVal) in enumerate(nsRange)
#     parameters[15] = nsVal
#     prob = ODEProblem(ribofinal!,init,tspan,(rates,parameters))
#     sol = solve(prob)
#     solDF = DataFrame([[j[i] for j in sol.u] for i=1:length(sol.u[1])], species)
#     lamValues[j,i] = calculateLambda(solDF[end, :])
#     mfa[j,i] = calculateMFAllocation(solDF[end, :])["Ribosomal"]
#   end
# end

# # lamdf = DataFrame(lamValues)
# plot(nsRange, lamValues, rhoRange, line_z=repeat(rhoRange, 1, length(nsRange))', linecolor=:blues, leg=false, cbar=true)
# xaxis!("ns")
# yaxis!("growth rate")
# title!("effect of rho on growth yield")

barkaiLR(x) =  0.35*x+0.08
barkaiRR(x) = 0.08/barkaiLR(x)
function errbaseFun(minimalRibofinal!,initConditions,minimalParameters)
    initConditions= init 
    tspan = (0.,1e5)
    error=0.0
    nsRange = 0.2:0.1:1
    for nsVal in nsRange
        global ns = nsVal
        prob = ODEProblem(minimalRibofinal!,initConditions,tspan,minimalParameters)
        sol = solve(prob, alg=Rosenbrock23(), abstol=1e-4, save_start=false, save_everystep=false; callback=cb)
        solDF = DataFrame([[j[i] for j in sol.u] for i=1:length(sol.u[1])], species)
        divRate = calculateDivRate(solDF[end,:])
        MFAllocation = calculateMFAllocation(solDF[end, :])["Ribosomal"]
        barkaiDivRate = barkaiLR(divRate)
        error += (MFAllocation - barkaiDivRate)^2
    end
    return error
end



#Random.seed!(1234)
errFun(theta) = errbaseFun(minimalRibofinal!,init,theta)
# rho, kp, thetar, kq, thetax, wr0, wq0, we0
sRange = [(1.0,7.0),(0.1,1e2),(0.1,6e3),(1e5,2e5),(1.0,20.0),(1e2,2e3),(0.1,1.3e3),(0.1,10.0)]
opts = bbsetup(errFun; Method = :adaptive_de_rand_1_bin_radiuslimited, SearchRange = sRange, NumDimensions = 5, MaxSteps = 1e4)

Random.seed!(100)
barkData = artificialdataset(30,80)
res = bboptimize(opts) 
#mulRes = [bboptimize(opts) for i=1:10]
# Best candidate
bestCandidate = [1.3383324073148009, 28.20274986976718, 5079.896327683835, 1.9215237109641056e6, 48.48799754476946, 243.29546852869055, 607.6246629776894, 0.519944909885737]

divRates = []
MFAllocations = []
lamValues = []
for nsVal in nsRange
    minimalParameters= best_candidate(res)
    global ns = nsVal
    prob = ODEProblem(minimalRibofinal!,init,tspan, minimalParameters)
    sol = solve(prob, abstol=1e-4,save_start=false, save_everystep=false)
    solDF = DataFrame([[j[i] for j in sol.u] for i=1:length(sol.u[1])], species)
    parameters[15] = nsVal
    push!(divRates, calculateDivRate(solDF[end,:]))
    push!(MFAllocations, calculateMFAllocation(solDF[end, :])["Ribosomal"])
    push!(lamValues, calculateLambda(solDF[end, :]))
end
plot(divRates,MFAllocations)
plot!(divRates, [barkaiLR(x) for x in divRates])

