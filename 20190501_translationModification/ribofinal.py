import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import odeint

def ribofinal(y, t, rates, parameters):
	dm= rates[0]
	kb= rates[1]
	ku= rates[2]

	Kp= parameters[0]
	thetar= parameters[1]
	kq= parameters[2]
	thetax= parameters[3]
	numr0= parameters[4]
	numq0= parameters[5]
	nume0= parameters[6]
	gmax= parameters[7]
	aatot= parameters[8]
	vt= parameters[9]
	Kt= parameters[10]
	s0= parameters[11]
	vm= parameters[12]
	Km= parameters[13]
	ns= parameters[14]
	nq= parameters[15]
	nr= parameters[16]
	nx= parameters[17]

	a= y[0]
	em= y[1]
	et= y[2]
	mm= y[3]
	mq= y[4]
	mr= y[5]
	mt= y[6]
	q= y[7]
	r= y[8]
	rm1m= y[9]
	rm1q= y[10]
	rm1r= y[11]
	rm1t= y[12]
	rm2m= y[13]
	rm2q= y[14]
	rm2r= y[15]
	rm2t= y[16]
	si= y[17]

	Kg= gmax/Kp
	gamma= gmax*a/(Kg + a)
	ttrate= (rm2q + rm2r + rm2t + rm2m)*gamma
	lam= ttrate/aatot
	nucat= em*vm*si/(Km + si)

	dydt= np.empty(18)
	dydt[0]= +ns*nucat-gamma/nr*rm2r*a-gamma/nx*rm2t*a-gamma/nx*rm2m*a-gamma/nx*rm2q*a-lam*a
	dydt[1]= +gamma/nx*rm2m*a-lam*em
	dydt[2]= +gamma/nx*rm2t*a-lam*et
	dydt[3]= +(nume0*a/(thetax + a))+ku*rm1m+7.2*rm1m-kb*r*mm-dm*mm-lam*mm
	dydt[4]= +(numq0*a/(thetax + a)/(1 + (q/kq)**nq))+ku*rm1q+7.2*rm1q-kb*r*mq-dm*mq-lam*mq
	dydt[5]= +(numr0*a/(thetar + a))+ku*rm1r+7.2*rm1r-kb*r*mr-dm*mr-lam*mr
	dydt[6]= +(nume0*a/(thetax + a))+ku*rm1t+7.2*rm1t-kb*r*mt-dm*mt-lam*mt
	dydt[7]= +gamma/nx*rm2q*a-lam*q
	dydt[8]= +ku*rm1r+ku*rm1t+ku*rm1m+ku*rm1q+gamma/nr*rm2r*a+gamma/nr*rm2r*a+gamma/nx*rm2t*a+gamma/nx*rm2m*a+gamma/nx*rm2q*a-kb*r*mr-kb*r*mt-kb*r*mm-kb*r*mq-lam*r
	dydt[9]= +kb*r*mm-ku*rm1m-7.2*rm1m-lam*rm1m
	dydt[10]= +kb*r*mq-ku*rm1q-7.2*rm1q-lam*rm1q
	dydt[11]= +kb*r*mr-ku*rm1r-7.2*rm1r-lam*rm1r
	dydt[12]= +kb*r*mt-ku*rm1t-7.2*rm1t-lam*rm1t
	dydt[13]= +7.2*rm1m-gamma/nx*rm2m*a-lam*rm2m
	dydt[14]= +7.2*rm1q-gamma/nx*rm2q*a-lam*rm2q
	dydt[15]= +7.2*rm1r-gamma/nr*rm2r*a-lam*rm2r
	dydt[16]= +7.2*rm1t-gamma/nx*rm2t*a-lam*rm2t
	dydt[17]= +(et*vt*s0/(Kt + s0))-nucat-lam*si

	return dydt

#######

# parameters
Kp= 7.0
thetar= 426.87
kq= 152219
thetax= 4.38
numr0= 930
numq0= 949
nume0= 4.38
gmax= 1260.0
aatot= 1.0e8
vt= 726.0
Kt= 1.0e3
s0= 1.0e4
vm= 5800.0
Km= 1.0e3
ns= 0.5
nq= 4
nr= 7549.0
nx= 300.0
parameters= np.asarray([ Kp, thetar, kq, thetax, numr0, numq0, nume0, gmax, aatot, vt, Kt, s0, vm, Km, ns, nq, nr, nx])

# define rate constants
dm= 0.1
kb= 1
ku= 1.0
rates= np.asarray([ dm, kb, ku])

# define initial conditions
a_0= 1000.0
em_0= 0
et_0= 0
mm_0= 0
mq_0= 0
mr_0= 0
mt_0= 0
q_0= 0
r_0= 10.0
rm1m_0= 0
rm1q_0= 0
rm1r_0= 0
rm1t_0= 0
rm2m_0= 0
rm2q_0= 0
rm2r_0= 0
rm2t_0= 0
si_0= 0
init= np.asarray([ a_0, em_0, et_0, mm_0, mq_0, mr_0, mt_0, q_0, r_0, rm1m_0, rm1q_0, rm1r_0, rm1t_0, rm2m_0, rm2q_0, rm2r_0, rm2t_0, si_0])

# call odeint (note args must be a tuple) 
t= np.linspace(0, 100, 10)
species= ['a', 'em', 'et', 'mm', 'mq', 'mr', 'mt', 'q', 'r', 'rm1m', 'rm1q', 'rm1r', 'rm1t', 'rm2m', 'rm2q', 'rm2r', 'rm2t', 'si']
y= odeint(ribofinal, init, t, args= (rates, parameters), mxstep= 10000)
