# parameters file

# define rate constants
const dm= 0.1
const kb= 1.
const ku= 1.0
rho= 0.5

Kp= 7.0 #parameterised
thetar= 426.87 # parameterised
kq= 152219. # parameterised
thetax= 4.38 # parameterised
wr0= 93. # parameterised
wq0= 949. # parameterised
we0= 4.38 # parameterised
const gmax= 1260.0
const aatot= 1.0e8
const vt= 726.0
const Kt= 1.0e3
const s0= 1.0e4
const vm= 5800.0
const Km= 1.0e3
global ns= 0.5
const nq= 4.
const nr= 7549.0
const nx= 300.0
const b = 0.
const cl= 0.
const k_cm = 0.00554752/662.435565 # parameterised

rates = [dm, kb, ku, rho]
parameters=  [Kp, thetar, kq, thetax, wr0, wq0, we0, gmax, aatot, vt, Kt, s0, vm, Km, ns, nq, nr, nx]
minimalParameters = [rho, Kp, thetar, kq, thetax, wr0, wq0, we0]

# define initial conditions
a_0= 1000.0
em_0= 0.
et_0= 0.
mm_0= 0.
mq_0= 0.
mr_0= 0.
mt_0= 0.
eq_0= 0.
er_0= 10.0
rm1m_0= 0.
rm1t_0= 0.
rm1q_0= 0.
rm1r_0= 0.
rm2m_0= 0.
rm2t_0= 0.
rm2q_0= 0.
rm2r_0= 0.
si_0= 0.
zmm_0= 0.
zmq_0= 0.
zmr_0= 0.
zmt_0= 0.

init= [a_0, em_0, et_0, mm_0, mq_0, mr_0, mt_0, eq_0, er_0, rm1m_0, rm1q_0, rm1r_0, rm1t_0, rm2m_0, rm2q_0, rm2r_0, rm2t_0, si_0, zmm_0, zmq_0, zmr_0, zmt_0]
