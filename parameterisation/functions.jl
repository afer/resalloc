
function calculateLambda(systemState)
    Kg= gmax/Kp
    a = systemState[:a]
    gamma= gmax*a/(Kg + a)
    ttrate= sum(systemState[[:rm2q, :rm2r, :rm2t, :rm2m]])*gamma
    
    return(ttrate/aatot)
    
end

function calculateDivRate(systemState)
    lam = calculateLambda(systemState)
   # Calculated in Divisions per hour 
    (60/(log(2)/lam))
end

function calculateTotalRNA(systemState)
    freeMRNA = sum(systemState[[:mm, :mq, :mr, :mt]])
    occupiedMRNA = sum(systemState[[:rm1m, :rm1q, :rm1r, :rm1t]])
    
    return(freeMRNA + occupiedMRNA)
end

function calculateWorkingRibosomes(systemState)
    rAtWork = sum(systemState[[:rm2q, :rm2r, :rm2t, :rm2m]])
    rNotAtWork = sum(systemState[[:er, :rm1m, :rm1q, :rm1r, :rm1t]])

    return(rAtWork /(rAtWork + rNotAtWork))
end

function testMassConservation(systemState)
    massr = sum(systemState[[:er, :rm1m, :rm1q, :rm1r, :rm1t, :rm2m, :rm2q, :rm2r, :rm2t]]) * 7549
    massx = sum(systemState[[:em, :et, :eq]]) * 300
    
    return(sum(massr) + sum(massx))
end

function calculateMFAllocation(systemState)
    em, et, eq = [systemState[CompartmentType] * nx for CompartmentType in [:em, :et, :eq]]
    er = sum(systemState[9:17]) * nr
    etot = testMassConservation(systemState)
    
    em = em/etot
    et = et/etot
    eq = eq/etot
    er = er/etot

    allocationDict = Dict()
    names =  ["Enzymatic","Transport", "Housekeeping", "Ribosomal"]
    allocatedPortion = [em, et, eq, er]
    
    for (allocation, name) in zip(allocatedPortion, names)
        allocationDict[name] = allocation
    end
    
    return allocationDict
end


# Wrapper function
function obtainData(systemState)
   println("Division rate (seconds): ", calculateDivRate(systemState)) 
   println("Total RNA: ", calculateTotalRNA(systemState))
   println("Ratio of working/not working ribosomes: ", calculateWorkingRibosomes(systemState)) 
   println("Total mass (Should approximate $(aatot)): ", testMassConservation(systemState)) 
   println(calculateMFAllocation(systemState)) 

end

